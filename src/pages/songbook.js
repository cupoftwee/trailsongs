import { withPageAuth, getUser } from '@supabase/auth-helpers-nextjs'
import DefaultLayout from '../components/layouts/default-layout'
import Image from 'next/image'
import Song from '../components/songbook/song'

export default function ProtectedPage({ user, customProp }) {
  const songsIds = ['1', '2', '3', '4']
  const songs = songsIds.map((songId, i) => {
    return <Song key="`${songId}--${i}`" />
  })
  return (
    <DefaultLayout>
      <div className="page-wrapper page-songbook">
        <article className="prose-stone px-24 py-12">
          <section className="header prose-xl">
            <h1>My Travels</h1>
          </section>

          <section className="description prose-lg">
            <h2>
              What&apos;s a Travelogue?
            </h2>
            <p>
              Your travelogue is a place to record all the adventures of you and your friends as you play Wanderhome. Each journey within your travelogue represents a different game played with a different cast of characters. 
            </p>
          </section>

          <section className="songs">
            <div className="songs-menu">
              <div className="prose-lg">
                <h2>
                  My Journeys
                </h2>
              </div>
              <ul className="flex gap-x-8 p-2 border-t border-b mb-4 border-stone-700">
                <li>
                  Sort
                </li>   
                <li>
                  New Journey
                </li>
                <li>
                  Join Journey
                </li>           
              </ul>
            </div>
            <div className="songs-list">
              <ul className="flex flex-wrap gap-6 justify-center">
                {songs}
              </ul>
            </div>
          </section>
        </article>
      </div>
    </DefaultLayout>
  )
}

export const getServerSideProps = withPageAuth({
  redirectTo: '/account',
  async getServerSideProps(ctx) {
    // Access the user object
    const { user, accessToken } = await getUser(ctx)
    return { props: { email: user?.email } }
  },
})