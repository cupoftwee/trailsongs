import { useUser } from '@supabase/auth-helpers-react'
import { supabaseClient } from '@supabase/auth-helpers-nextjs'
import { useEffect, useState } from 'react'
import DefaultLayout from '../components/layouts/default-layout'
import AccountAuth from '../components/account/account-auth'

const AccountPage = () => {
  const { user, error } = useUser()
  const [data, setData] = useState()

  useEffect(() => {
    async function loadData() {
      const { data } = await supabaseClient.from('test').select('*')
      setData(data)
    }
    // Only run query once user is logged in.
    if (user) loadData()
  }, [user])

  if (!user)
    return (
      <DefaultLayout>
        <AccountAuth error={error} />
      </DefaultLayout>
    )

  return (
    <DefaultLayout>
      <article className="prose prose-stone prose-lg mx-auto py-12">
        <h1>My Account</h1>
        <p>user:</p>
        <pre>{JSON.stringify(user, null, 2)}</pre>
        <p>client-side data fetching with RLS</p>
        <pre>{JSON.stringify(data, null, 2)}</pre>

        <button onClick={() => supabaseClient.auth.signOut()}>Log out</button>
      </article>
    </DefaultLayout>
  )
}

export default AccountPage