import Link from "next/link"

export default function NavFooter() {
  return (
    <div className="footer-module min-h-[17vh] bg-stone-600 text-stone-300 border border-stone-400">
      <footer className="w-full container mx-auto px-8 py-8 flex justify-between">
        <ul className="flex flex-col gap-y-2">
          <li className="nav-link">
            <Link href="/songbook">About</Link>
          </li>
          <li className="nav-link">
            <a href="/community">Community</a>
          </li>
        </ul>
        
        <ul className="flex flex-wrap">
          <li className="nav-link w-1/2">
            <Link href="/songbook">My Travelogue</Link>
          </li>
          <li className="nav-link w-1/2">
            <a href="/content-manager">My Content</a>
          </li>
          <li className="nav-link w-1/2">
            <Link href="/account">My Account</Link>
          </li>
          <li className="nav-link w-1/2">
            <Link href="/account/settings">My Settings</Link>
          </li>
        </ul>

        <div className="logo logo-footer font-serif ffont-serif font-semibold italicont-semibold italic text-2xl -mt-1">
          <Link href="/">Trailsongs</Link>
        </div>
      </footer>
    </div>
  )
}