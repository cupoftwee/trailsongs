import Link from 'next/link'
import { useUser } from '@supabase/auth-helpers-react'
import { supabaseClient } from '@supabase/auth-helpers-nextjs'

export default function NavHeader() {
  const { user, error } = useUser()

  const unAuthenticatedMenu = (
    <ul className="flex gap-x-6">
      <li className="nav-link">
        <Link href="/account">
          Log In / Sign Up
        </Link>
      </li>
    </ul>
  )
  const authenticatedMenu = (
    <ul className="flex gap-x-6">
      <li className="nav-link">
        <Link href="/songbook">
          My Travelogue
        </Link>
      </li>
      <li className="nav-link">
        <a href="/content-manager">
          My Content
        </a>
      </li>
      <li className="nav-link">
        <Link href="/account">
          My Account
        </Link>
      </li>
      <li className="nav-link">
        <a 
          className="cursor-pointer" 
          onClick={() => supabaseClient.auth.signOut()}
        >
          Log Out
        </a>
      </li>
    </ul>
  )
  const mainMenu = user ? authenticatedMenu : unAuthenticatedMenu

  return (
    <div className="nav-module min-h-[8vh] bg-stone-300 text-stone-600 border-b border-stone-500 ">
        <nav className="w-full container mx-auto px-6 py-4 flex justify-between">
          <div className="logo logo-header font-semibold font-italic font-serif text-2xl -mt-1">
            <Link href="/">Trailsongs</Link>
          </div>

          <ul className="flex gap-x-6">
            <li className="nav-link">
              <Link href="/songbook">
                About
              </Link>
            </li>
            <li className="nav-link">
              <Link href="/community">
                Community
              </Link>
            </li>
          </ul>

          {mainMenu}
        </nav>
      </div>
  )
}