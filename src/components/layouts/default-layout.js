import Head from "next/head"
import NavHeader from "../nav/nav-header"
import NavFooter from "../nav/nav-footer"

export default function DefaultLayout({ children }) {
  return (
    <div className="app-wrapper">
      <Head>
        <title>Trailsongs | a Wanderhome companion app</title>
        <meta name="description" content="Trailsongs | a Wanderhome companion app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <NavHeader />

      <main className="min-h-[75vh] bg-stone-200">
        {children}
      </main>

      <NavFooter />
    </div>
  )
}