import { Auth } from '@supabase/ui'
import { supabaseClient } from '@supabase/auth-helpers-nextjs'

const AccountAuth = (props) => {
  return (
    <>
     <div className="py-12 container mx-auto px-24">
      {props?.error && <p>{props?.error?.message}</p>}
        <Auth
          supabaseClient={supabaseClient}
          providers={['google', 'github', 'discord']}
          socialLayout="horizontal"
          socialButtonSize="xlarge"
        />
     </div>
    </>
  )
}

export default AccountAuth