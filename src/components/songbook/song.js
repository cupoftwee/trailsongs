export default function Song () {
  return (
    <li className="song-wrapper bg-stone-400 rounded w-[48%]">
      <div className="song prose-stone prose p-4">
        <h3>
          Journey Name
        </h3>

        <ul className="flex justify-start list-none pl-0 mb-0">
          <li className="w-1/2 pl-0">
            <p className="song-season my-0">
              <b>Season:</b> Season Name
            </p>
          </li>
          <li className="w-1/2 pl-0">
            <p className="song-month my-0">
              <b>Month:</b> Month Name
            </p>
          </li>
        </ul>

        <p className="song-sessions my-0">
          <b>Location:</b> Location Name
        </p>
        <p className="song-sessions my-0">
          <b>Sessions:</b> XXX
        </p>
        <p className="song-last-update my-0">
          <b>Last Updated:</b> Sunday, October 2nd, 2022
        </p>

        <p className="font-bold">Cast Members</p>
        <ul className="song-cast flex list-none">
          <li>
            <picture>
              <source 
                srcSet="https://via.placeholder.com/20" 
                type="image/png" />
              <img 
                src="https://via.placeholder.com/20" 
                alt="username profile image alt" />
            </picture>
          </li>
          <li>
            <picture>
              <source 
                srcSet="https://via.placeholder.com/20" 
                type="image/png" />
              <img 
                src="https://via.placeholder.com/20" 
                alt="username profile image alt" />
            </picture>
          </li>
          <li>
            <picture>
              <source 
                srcSet="https://via.placeholder.com/20" 
                type="image/png" />
              <img 
                src="https://via.placeholder.com/20" 
                alt="username profile image alt" />
            </picture>
          </li>
        </ul>
      </div>
    </li>
  )
}